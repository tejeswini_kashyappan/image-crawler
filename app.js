var phantom = require('phantom');
var $ = require('cheerio');
var fs = require('fs');
var lineReader = require('readline').createInterface({ input: fs.createReadStream('mydataset.txt') });
var fcount=0;
var dataset=[];
var maxLoops = 0;

console.log('Preparing keywords...');
lineReader.on('line', function (line) {
  fcount++;
  var keywords = line.split(",");
  var wcount=0;
  keywords.forEach(function(item,index) {
     maxLoops++;
     wcount++;
     dataset.push(fcount+"$$$"+wcount+"$$$"+item.trim());
  });
});

setTimeout(function(){
var counter=0;
(function next(){
  if(counter++ >= maxLoops) return;
  setTimeout(function(){
  var data = dataset[counter-1].split("$$$");
  var result = getImageList(data[0], data[1], data[2],function(f,w,i,image)
    {
	   var imagePath = "images/f"+f+"/f"+f+"_"+w+""+i+".jpg "+f+" "+image+"\r\n";
	   fs.appendFile('datasetUrl.txt',imagePath,function(err){
  		//save urls
	   });
    });
  next();
  }, 3000);
})();
},10000);


function getImageList(f, w, keyword, callback)
{
 phantom.create().then(function(ph) {
    ph.createPage().then(function(page) {
    page.setting('userAgent','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36');
    page.open('https://www.google.com/search?site=imghp&tbm=isch&source=hp&biw=1414&biw=709&q='+keyword+'&oq='+keyword).then(function(){
    	page.property('content').then(function(content) {
	console.log(keyword);
	   var parsedHTML = $.load(content);
	   var i = 0;
	   parsedHTML('a.rg_l').map(function(i, link) {
		i=i+1;
		var href = $(link).attr('href');
		var img = href.replace("/imgres?imgurl=","");
		img = img.replace("http://images.google.de","");
		var imgurl = decodeURIComponent((img.split('&'))[0]);
		callback(f,w,i,imgurl);
	   });	   
	ph.exit();
     });
   });
 });
});
}
