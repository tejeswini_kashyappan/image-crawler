import urllib 
import os
import imghdr
import socket

file_name = raw_input("Enter the dataset url file: ")
train_im = raw_input("How many training images you want to download for each keyword? : ")
val_im = raw_input("How many validation images you want to download for each keyword? : ")
train_im=int(train_im)
val_im=int(val_im)
print("Please wait while downloading images...")
val_txt = open("val.txt","w")
train_txt = open("train.txt","w")
counter=0
i=0
with open(file_name,"rw+") as f:
	for line in f:
		data = line.split(" ")
		path = data[0]
		group = int(data[1])
		url = data[2]
		temp = data[0].split("/")
		folder = temp[1]
		filename = temp[2]
		if i==group:
		   counter+=1
		else:
		   i=group
		   counter=0

		if not os.path.exists("train/"+str(folder)):
			os.makedirs("train/"+str(folder))
		
		if counter < train_im:
		  imagePath = "train/"+str(folder)+"/"+str(filename)+".jpg"
		  socket.setdefaulttimeout(10)
	   	  try:
			urllib.urlretrieve(url,imagePath)
			if imghdr.what(imagePath) != "jpeg":
				os.remove(imagePath)
			else:
				train_txt.write(imagePath+" "+str(group)+"\n")
	    	  except:
	        	pass
		
		if counter >= train_im and counter < val_im+train_im:
		  imagePath = "val/"+str(filename)+".jpg"
		  socket.setdefaulttimeout(10)
	   	  try:
			urllib.urlretrieve(url,imagePath)
			if imghdr.what(imagePath) != "jpeg":
				os.remove(imagePath)
			else:
				val_txt.write(imagePath+" "+str(group)+"\n")
	    	  except:
	        	pass

